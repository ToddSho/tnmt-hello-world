from flask import Flask, request, make_response
from flask.ext.restful import reqparse,abort, Resource, Api
 
def any_response(data):
  ALLOWED = ['*']
  response = make_response(data)
  origin = request.headers['Origin']
  if origin in ALLOWED:
    response.headers['Access-Control-Allow-Origin'] = origin
  return response

app = Flask(__name__)
api = Api(app)

TMNT =  {
        'turtle1' : {'id':1,'name':'Donatello','color':'purple'},
        'turtle2' : {'id':2,'name':'Michelangelo','color':'orange'},
        'turtle3' : {'id':3,'name':'Leonardo','color':'blue'},
        'turtle4' : {'id':4,'name':'Raphael','color':'red'},
        }

def abort_if_turtle_doesnt_exist(turtle_id):
    if turtle_id not in TMNT:
        abort(404, message="{} doesn't exist".format(turtle_id))
    
#     parser = reqparse.RequestParser()
#     parser.add_argument('id', type=int)
#     parser.add_argument('name', type=str)
#     parser.add_argument('color', type=str)


class Tmnt(Resource):
    def get(self):
        return TMNT
    def post(self):
        args = parser.parse_args()
        turtle_id = 'turtle%d' % (len(TMNT) + 1)
        tmnt[turtle_id] = {'id':args['id'],'name':args['name'],'color':args['color']}

class Turtle(Resource):
    def get(self, turtle_id):
        abort_if_turtle_doesnt_exist(turtle_id)
        return TMNT[turtle_id]
        
    def delete(self, turtle_id):
        abort_if_turtle_doesnt_exist(turtle_id)
        del TMNT[turtle_id]
        return '', 204
        
    def put(self, turtle_id):
        args = parser.parse_args()
        turtle = {'id':args['id'],'name':args['name'],'color':args['color']}
        TMNT[tutle_id] = turtle
        return turtle,201
        
api.add_resource(Tmnt, '/')
api.add_resource(Turtle, '/<string:turtle_id>')

if __name__ == '__main__':
    app.run(debug=True)